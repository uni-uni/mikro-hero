using System;

namespace MikroHero
{
    public class Game
    {
        private APlayer _player;
        private Enemy _enemy = new Enemy();
        private bool _lastTourWon = false;
        
        public Game(){
            Init();
            LoopTheGame();
        }
        private void Init()
        {
            Console.Clear();
            Console.WriteLine("Wybierz swoja plec 1. kobieta 2. mezczyzna");
            var gender = Console.ReadLine();
            switch (gender)
            {
                case "1":
                    _player = PlayerFactory.MakePlayer("female");
                    break;
                case "2":
                    _player = PlayerFactory.MakePlayer("male");
                    break;
                default:
                    throw new Exception("No such gender!");
            }
        }

        private void ShowTourResult(){
            if(_lastTourWon)
            Console.WriteLine("Wygrales walke");
            else
            Console.WriteLine("Przeciwnik wygral walke");
        }
        
        private void LoopTheGame()
        {
            int WinCounter = 0;
            
            while(WinCounter < 5)
            {
                Console.WriteLine("Czy zawalczyc o swoja {0}, wygrana? Do zwyciezstwa potrzbujesz jeszcze {1}.", WinCounter+1, 5-WinCounter);
                Console.WriteLine("1. Chce      2. Nie chce, koncze gre");
                int answer = int.Parse(Console.ReadLine());
                Console.Clear();
                if( answer == 1)
                {
                    fight();
                    if(_lastTourWon)
                {
                    _player.IncreaseBonusHealthPoints();
                    WinCounter++;
                }
                else
                {
                    _player.DecreaseBonusHealthPoints();
                }

                ResetTour();
                }
                else {
                    Console.WriteLine("Dziekujemy za gre!");
                    break;
                }
            }
        
           Console.Clear();
           Console.WriteLine("Gratulueje wojowniku, pokonales wystarczajaca liczbe przeciwnikow by pojsco do Valhalli!");
        }

        private void ResetTour()
        {
            _enemy.ResetHealthPoints();
            _player.ResetHealthPoints();
        }


        private void fight()
        {
            bool isFightOver = false;

            while(!isFightOver)
            {
                var damageDealt=_player.Damage();
                var healthLeft = _enemy.TakeDamage(damageDealt);
                
                if(!_enemy.IsAlive())
                {
                    Console.Clear();   
                    Console.WriteLine("Przeciwnik zostal pokonany!");
                    isFightOver = true;
                    _lastTourWon = true;
                    break;
                }
                else
                {
                    Console.WriteLine("i zostawiles mu {0} hp", healthLeft);
                }

                damageDealt = _enemy.Damage();
                healthLeft = _player.TakeDamage(damageDealt);
            
                if(!_player.IsAlive())
                {
                    Console.Clear();
                    Console.WriteLine("Zostales pokonanny!");
                    isFightOver = true;
                    _lastTourWon = false;
                    break;
                }
                else
                {
                    Console.WriteLine("Przeciwnik zadal Ci cios za {0} punktow zdrowia, zostalo Ci {1} hp",
                    damageDealt, healthLeft);
                }
                Console.WriteLine("");
            }
            
        }
    }
}