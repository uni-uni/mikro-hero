namespace MikroHero
{
    public interface IHero
    {
        int HealthPoints { get; set; }

        int Damage();
        int TakeDamage(int dmg);
        bool IsAlive();
        void ResetHealthPoints();
    }
}