using System;

namespace MikroHero
{
    public static class PlayerFactory
    {
        public static APlayer MakePlayer(string playerType)
        {
            return playerType switch
            {
                "male" => new PlayerMale(),
                "female" => new PlayerFemale(),
                _ => throw new Exception("No such playerType")
            };
        }
    }
}