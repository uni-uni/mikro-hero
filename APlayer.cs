using System;

namespace MikroHero
{
    public abstract class APlayer : IHero
    {
        private int _bonusHealthPoints = 0;
        public int HealthPoints { get; set; } = 100;

        protected virtual void Greeting()
        {
            System.Console.WriteLine("Witaj wojowniku!");
        }

        public int Damage()
        {
            var attackType = ChooseAttackType();

            switch (attackType)
            {
                case 1:
                    var swordDmg = WeaponContext.UseWeapon(new SwordStrategy(50));
                    Console.Write("Uderzasz mieczem przez co zabierasz {0} punktow zycia ", swordDmg);
                    return swordDmg;
                    break;
                case 2:
                    var fireballDmg = WeaponContext.UseWeapon(new FireballStrategy(20));
                    Console.Write("Rzucasz kulę ognia przez co zadajesz {0} punktow zycia ", fireballDmg);
                    return fireballDmg;
                    break;
                default:
                    throw new Exception("No such attackType");
            }
        }

        public bool IsAlive()
        {
            return HealthPoints > 0 && _bonusHealthPoints > -100;
        }

        public int TakeDamage(int dmg)
        {
            HealthPoints -= dmg;
            return HealthPoints;
        }

        public void ResetHealthPoints()
        {
            HealthPoints = 100 + _bonusHealthPoints; 
        }

        // TODO set right value
        public void DecreaseBonusHealthPoints()
        {
            _bonusHealthPoints -= 4;
        }
        
        // TODO  set right value
        public void IncreaseBonusHealthPoints()
        {
            _bonusHealthPoints += 4;
        }

        private static int ChooseAttackType()
        {
            Console.WriteLine("Wybierz atak:");
            Console.WriteLine("1. Atak mieczem (100% szans na obrażania)");
            Console.WriteLine("2. Rzucenie zaklęcia (szansa na obrażenia krytyczne lub bardzo małe)"); 
            var attackType = int.Parse(Console.ReadLine());
            Console.Clear();

            while (!IsAttackTypeCorrect(attackType))
            {
                Console.WriteLine("Atak nie istnieje! Wybierz ponownie:");
                 attackType =int.Parse(Console.ReadLine());
            }
            
            return attackType;
        }

        private static bool IsAttackTypeCorrect(int attackType)
        {
            return (attackType == 1 || attackType == 2);
        }
    }
}